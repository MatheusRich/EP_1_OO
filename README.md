---> PASTAS:


bin: Local onde fica armazenado o arquivo binário.

doc: Local onde estão armazenadas as imagens a serem decifradas.

inc: Local onde estão armazenados os Headers.

obj: Local onde estão os arquivos objetos.

src: Local onde estão armazenadas as implementações dos headers e a main.




---> COMO COMPILAR E EXECUTAR:


- Primeiramente abra o terminal, entre na pasta EP_1_OO.

- Digite "make clean" antes de tudo.

- Digite "make" para compilar o projeto.

- Digite "make run" para executar o projeto.




---> UTILIZANDO O PROGRAMA:


- Primeiramente o programa solicitará o nome.formato da imagem. OBS: Válidas apenas imagens contidas na pasta DOC.
EXEMPLOS: "imagem1.pgm", "mensagem2.ppm", "lena.pgm".

- Caso a imagem não seja encontrada, o programa exibirá uma mensagem de erro. Caso contrário, o programa exibirá uma mensagem de sucesso.

- Em seguida, o programa identificará o formato da imagem e executará comandos diferentes de acordo com o formato.

--PGM:


- Se o programa identificar a imagem como .pgm, ele pedirá que o usuário insira a posição inicial da imagem. OBS: na imagem lena.pgm a posição inicial da mensagem é 50000, porém neste local há um símbolo '#', portanto o programa não executará corretamente, recomenda-se que se use a posição "50008" para evitar esse erro.
EXEMPLOS: "5000", "50000".

- Por fim, o programa exibirá a mensagem escondida, e finalizará.


--PPM:


- Se o programa identifar a imagem como .ppm, ele pedirá que o usuário insira um local para salvar a nova imagem com o filtro aplicado. OBS: Não digite '/' depois do último local do arquivo.
EXEMPLOS: "doc", "/home/matheus_richard/Downloads".

- Em seguida o programa o programa exibirá um menu com as opções de filtros a serem aplicados. OBS: Se um valor não válido for inserido, o programa voltará a solicitar uma opção válida.

- Por fim o programa exibirá uma mensagem de erro caso a nova imagem não tenha sido criada, ou uma mensagem de sucesso, caso a imagem tenha sido criada.

- Em seguida o programa se encerrará.