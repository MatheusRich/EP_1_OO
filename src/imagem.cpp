#include "imagem.hpp"
#include <fstream>


using namespace std;

Imagem::Imagem()
{

}

Imagem::~Imagem()
{

}

char *Imagem::abrirImagem()
{	
	dimensaoTotalImagem=2;
	int contador=0;
	char caractere=0;
	char *imagem=0;

		cout<<"Digite nome.formato da imagem: "<<endl;
		cin>>nomeArquivo;
		local = "doc/" +nomeArquivo;
		fstream arquivo(local.c_str());		
		if (arquivo == NULL)
		{
			sleep(1);
			throw ('A');

		}

		else
		{
			cout<<endl<<"O arquivo foi encontrado e aberto com sucesso!"<<endl<<endl;
				sleep(1);
				getline(arquivo,numeroMagico);
				getline(arquivo,comentario);				
				arquivo>>altura;
				arquivo>>largura;
				arquivo>>escalaDeCinza;
				dimensaoTotalImagem = altura * largura;	

				imagem = new char [dimensaoTotalImagem];
				//Copiando os caracteres da imagem para um array.
				do
				{

					arquivo.get(caractere);
					imagem[contador]= caractere;
					contador++;
				}while(contador<dimensaoTotalImagem);
				
		}

	return imagem;
}


string Imagem::getNumeroMagico()
{
	return numeroMagico;
}

string Imagem::getComentario()
{
	return comentario;
}

string Imagem::getLocalImagem()
{
	return local;
}

int Imagem::getAltura()
{
	return altura;
}

int Imagem::getLargura()
{
	return largura;
}

int Imagem::getEscalaDeCinza()
{
	return escalaDeCinza;
}

int Imagem::getDimensaoTotalImagem()
{
	return dimensaoTotalImagem;
}

void Imagem::setNumeroMagico(string numeroMagico)
{
	this->numeroMagico = numeroMagico;
}

void Imagem::setAltura(int altura)
{
	this->altura = altura;
}

void Imagem::setLargura(int largura)
{
	this->largura = largura;
}

void Imagem::setEscalaDeCinza(int escalaDeCinza)
{
	this->escalaDeCinza = escalaDeCinza;
}