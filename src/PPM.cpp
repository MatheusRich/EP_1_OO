#include "PPM.hpp"

PPM::PPM()
{	

}

PPM::PPM(string numeroMagico,string comentario, int altura, int largura,int escalaDeCinza,int dimensaoTotalImagem)
{
	this->numeroMagico=numeroMagico;
	this->comentario=comentario;
	this->altura=altura;
	this->largura=largura;
	this->escalaDeCinza=escalaDeCinza;
	this->dimensaoTotalImagem=dimensaoTotalImagem;
}

PPM::~PPM()
{	

}

char* PPM::abrirImagem(string local)
{
	int contador=0;
	char *imagem;
	char caractere;

	fstream imagemAberta(local.c_str());

	getline(imagemAberta,numeroMagico);
	getline(imagemAberta,comentario);				
	imagemAberta>>altura;
	imagemAberta>>largura;
	imagemAberta>>escalaDeCinza;
	dimensaoTotalImagem = altura * largura * 3;// 3 vezes mais porque são 3 valores pra cada pixel.	

	imagem = new char [dimensaoTotalImagem];
	//Copiando os caracteres da imagem para um array.
	do
		{

			imagemAberta.get(caractere);
			imagem[contador+2]= caractere;
			contador++;
		}while(contador<dimensaoTotalImagem);
			
	return imagem;
}

string PPM::getLocalNovaImagem()
{
	return localNovaImagem;
}


void PPM::decifraImagem(char *imagem)
{
	int contador=0,i=0;
	

	cout<<"Digite local onde deseja salvar a nova imagem: "<<endl;
	cin>>localParcial;
	localNovaImagem = localParcial+ "/filtro_aplicado.ppm";
	ofstream novaImagem(localNovaImagem.c_str());

	//Menu de Filtros
	cout<<endl<<endl<<"________________________________"<<endl<<"Escolha o filtro a ser aplicado:"<<endl<<endl<<"1 - Red"<<endl<<"2 - Green"<<endl<<"3 - Blue"<<endl<<"________________________________"<<endl<<endl<<"Opção: ";
	cin>>filtro;

	while(filtro != 1 && filtro != 2 && filtro != 3)
	{
		cout<<endl<<"________________________________"<<endl<<"ERRO: Número digitado inválido."<<endl<<"Tente novamente:"<<endl<<endl<<"1 - Red"<<endl<<"2 - Green"<<endl<<"3 - Blue"<<endl<<"________________________________"<<endl<<endl<<"Opção: ";
		cin>>filtro;
	}
	
	//Escrevendo o cabeçalho da nova imagem
	novaImagem<<numeroMagico;
	novaImagem<<endl;
	novaImagem<<comentario;
	novaImagem<<endl;
	novaImagem<<altura;
	novaImagem<<" ";
	novaImagem<<largura;
	novaImagem<<endl;
	novaImagem<<escalaDeCinza;
	novaImagem<<endl;


	//Escrevendo os pixels da nova imagem
	do
	{	//Filtro
		for(i=1;i<=3;i++)
		{
			if(i!=filtro)
				{
					novaImagem<<0;
				}
			else
				{
					novaImagem<<imagem[contador];		
				}
				contador++;				
		}



	}while(contador<(dimensaoTotalImagem));
	cout<<endl<<endl;
}

void PPM::verificaImagem(string localNovaImagem)
{
	fstream verificador(localNovaImagem.c_str());
	if (verificador == NULL)
		{
			sleep(1);
			cout<<"ERRO: A imagem não pôde ser criada."<<endl;

		}

		else
		{	sleep(1);
				cout<<endl<<"A imagem foi criada com sucesso!"<<endl<<endl;
		}

		
}