#include "imagem.hpp"
#include "PGM.hpp"
#include "PPM.hpp"

using namespace std;

int main()
{	
	char *imagemTeste;
	PGM teste;
	
	//Verificando se a imagem inserida existe
	try
	{
		imagemTeste = teste.abrirImagem();
	}
	catch (char A)
	{
		cout<<"ERRO: Arquivo não encontrado."<<endl;
		return -1;
	}
	
	//Caso seja uma imagem PGM
	if(teste.getNumeroMagico() == "P5")
	{
		PGM pgm;
		pgm.decifraImagem(imagemTeste);
	}
	//Caso seja uma imagem PPM
	else if(teste.getNumeroMagico() == "P6")
	{	//Construtor PPM
		PPM ppm(teste.getNumeroMagico(),teste.getComentario(),teste.getAltura(),teste.getLargura(),teste.getEscalaDeCinza(),teste.getDimensaoTotalImagem());
		char *imagemPPM;
		//Abrindo a imagem usando o método sobrescrito.
		imagemPPM = ppm.abrirImagem(teste.getLocalImagem());
		//Aplicando os filtros.
		ppm.decifraImagem(imagemPPM);
		//Verificando se a imagem foi criada.
		ppm.verificaImagem(ppm.getLocalNovaImagem());			
	}

	return 0;
}