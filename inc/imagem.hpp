#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <unistd.h>

using namespace std;

class Imagem
{
protected:
	string numeroMagico;
	string comentario;
	string nomeArquivo;
	string local;
	int largura;
	int altura;
	int escalaDeCinza;
	int dimensaoTotalImagem;
public:
	Imagem();
	~Imagem();		
	char *abrirImagem();
	void decifraImagem();
	string getNumeroMagico();
	string getComentario();
	string getLocalImagem();
	int getAltura();
	int getLargura();
	int getEscalaDeCinza();
	int getDimensaoTotalImagem();
	void setNumeroMagico(string numeroMagico);
	void setAltura(int altura);
	void setLargura(int largura);
	void setEscalaDeCinza(int escalaDeCinza);
};
#endif