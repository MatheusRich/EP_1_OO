#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

class PPM : public Imagem
{
private:

	int filtro;
	string localParcial,localNovaImagem;

public:
	PPM();
	PPM(string numeroMagico,string comentario, int altura, int largura,int escalaDeCinza,int dimensaoTotalImagem);
	~PPM();

	char *abrirImagem(string local);
	string getLocalNovaImagem();
	void decifraImagem(char *imagem);
	void verificaImagem(string localNovaImagem);
};
#endif