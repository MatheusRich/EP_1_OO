#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

using namespace std;

class PGM : public Imagem
{
private:

	char bitExtraido;
	char byteExtraido;
	int posicaoInicial;

public:
	
	PGM();
	~PGM();
	void decifraImagem(char *imagem);
	
};


#endif